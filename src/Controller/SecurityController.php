<?php

namespace App\Controller;

use App\Form\LoginType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'security_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        // return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        $form = $this->createForm(LoginType::class, []);
    
        return $this->render('security/login.html.twig', [
            'formView' => $form->createView(),
        ]);
    }

    // public function login(Request $request): Response
    // {
        // $form = $this->createForm(LoginType::class, []);
    
        // return $this->render('security/login.html.twig', [
        //     'formView' => $form->createView(),
        // ]);
    // }

    #[Route('/logout', name: 'security_logout')]
    public function logout()
    {
    }
}
