<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Flex\Recipe;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;

class LoginFormAuthenticator extends AbstractAuthenticator
{
    protected $client;
    // protected $user;

    // public const LOGIN_ROUTE = 'security_login';

    // private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator, )
    {
        $this->client = HttpClient::create(['verify_peer' => false, 'verify_host' => false]);
        // $this->user = new User();
        // $this->urlGenerator = $urlGenerator;
        $this->response = [];

    }
//     protected function getLoginUrl(Request $request): string
// {
//     return $this->urlGenerator->generate(self::LOGIN_ROUTE);
// }

    public function supports(Request $request): ?bool
    {
        // //On vérifie si on a besoin d'appeler l'authenticator
       if($request->attributes->get('_route') === 'security_login'
               && $request->isMethod('POST')) {

                $data = $request->request->get('login');
                      $array = [
                           'username' => $data['username'],
                           'password' => $data['password'],
                       ];
                    //   $array = [
                    //        'username' => $data->get('username'),
                    //        'password' => $data->get('password'),
                    //    ];
                       
                       $body= json_encode($array);
               
               
                       $this->response = $this->client->request(
                           'POST',
                           'https://127.0.0.1:8001/login', 
                           [
                               'headers'=> [
                                   'Content-Type' => 'application/json'
                               ],
                               'body' => $body
                           ]
                       );
                       //     $statusCode = $response->getStatusCode();
                   // $statusCode = 200
                   $contentType = $this->response->getHeaders()['content-type'][0];
                   // $contentType = 'application/json'
                   $content = $this->response->getContent();
                   // $content = '{"id":521583, "name":"symfony-docs", ...}'
                   $content = $this->response->toArray();
                   // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]
           
                   return $this->response->getHeaders()['x-debug-token'][0] !== null ;
               }
               else {
                   return false;
               }
    }

    public function authenticate(Request $request): PassportInterface
    {
        $apiToken = $this->response->getHeaders()['x-debug-token'][0];
        if (null === $apiToken) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }

        return new SelfValidatingPassport(new UserBadge($apiToken));
    //     $data = $request->request;
    // //    $array = [
    // //         'username' => $data['username'],
    // //         'password' => $data['password'],
    // //     ];
    //    $array = [
    //         'username' => $data->get('username'),
    //         'password' => $data->get('password'),
    //     ];
        
    //     $body= json_encode($array);


    //     $response = $this->client->request(
    //         'POST',
    //         'https://127.0.0.1:8001/login', 
    //         [
    //             'headers'=> [
    //                 'Content-Type' => 'application/json'
    //             ],
    //             'body' => $body
    //         ]
    //     );
    //     $statusCode = $response->getStatusCode();
    //     // $statusCode = 200
    //     $contentType = $response->getHeaders()['content-type'][0];
    //     // $contentType = 'application/json'
    //     $content = $response->getContent();
    //     // $content = '{"id":521583, "name":"symfony-docs", ...}'
    //     $content = $response->toArray();
    //     // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

    //     $tokenDeLespoir = $response->getHeaders()['x-debug-token'][0];
    //     $apiToken = $content['token'];

    //     if (null === $apiToken || null === $tokenDeLespoir) {
    //         // The token header was empty, authentication fails with HTTP Status
    //         // Code 401 "Unauthorized"
    //         throw new CustomUserMessageAuthenticationException('No API token provided');
    //     }
      
    //     // $username = $data->get('username');
    //     // $password = $data->get('password');

    //     // $this->user->setPassword($password);
    //     // $this->user->setUsername($username);
    //     // $this->user->setToken($apiToken);
    // //    dd($username, $password, $apiToken);
    //     return new SelfValidatingPassport(new UserBadge($tokenDeLespoir));

    //     //  $passport= new SelfValidatingPassport(new UserBadge($username)
    //     //     );
    //     // //  $passport->setAttribute('apiToken', $apiToken);
    //     // //  dd($passport);
    //     // return $passport;
    //     // dd( new Passport(
    //     //     new UserBadge($username),
    //     //     new PasswordCredentials($password),
    //     //     [
    //     //         new CsrfTokenBadge('authenticate', $request->get('_csrf_token')),
    //     //     ]
    //     //     ));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // dd("ça marche");
        // //on success let the request continue
        // return new RedirectResponse('/');

        // on success, let the request continue
        return null;

    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {

        dd($exception);
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

//    public function start(Request $request, AuthenticationException $authException = null): Response
//    {
//        /*
//         * If you would like this class to control what happens when an anonymous user accesses a
//         * protected page (e.g. redirect to /login), uncomment this method and make this class
//         * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntrypointInterface.
//         *
//         * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
//         */
//    }
}
